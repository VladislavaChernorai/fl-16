function isEquals(a, b) {
    return a === b
}

function isBigger(a, b) {
    return a > b
}

function storeNames(...names) {
    let result = []
    for (let temp of names)
    {
        result.push(temp)
    }

    return result
}

function getDifference(a, b) {
    let result = a - b;
    if(result < 0)
    {
        result *= -1
    }

    return result
}

function negativeCount(arr) {
    let result = 0
    for(let temp of arr)
    {
        temp < 0 ? result++ : result
    }

    return result
}

function letterCount(str, letter) {
    let result = 0;

    str = str.toLowerCase()
    letter = letter.toLowerCase()

    for(let temp of str)
    {
        temp === letter ? result++ : result
    }

    return result
}

function countPoints(arr) {
    let result = 0

    let a
    let b
    for(let score of arr) {
        a = +score.split(':')[0]
        b = +score.split(':')[1]

        if(a > b) {
            result += 3
        } else if(a === b) {
            result += 1
        }
    }
    
    return result
}