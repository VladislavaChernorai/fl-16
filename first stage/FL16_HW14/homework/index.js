
let elements = document.getElementsByClassName('listener');


let rows = document.getElementsByClassName('row');

function validatePhoneNumber(input_str) {
    let re = /^[\]?[(]?[0-9]{3}[)]?[-\s\]?[0-9]{3}[-\s\]?[0-9]{4,6}$/im;

    return re.test(input_str);
}

function validateForm(event) {
    let phone = document.getElementById('tel').value;
    if (!validatePhoneNumber(phone)) {
        document.getElementById('form_error').classList.remove('hidden');
        document.getElementById('form_success').classList.add('hidden');

    } else {
        document.getElementById('form_error').classList.add('hidden');
    }
    event.preventDefault();
}

document.getElementById('form').addEventListener('submit', validateForm);

let div = document.getElementById('ball');

