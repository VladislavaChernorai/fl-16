function reverseNumber(num) {
    let result = ''
    if (num < 0) {
        result = '-'
        num *= -1
    }
    num += ''
    for (let i = num.length - 1; i >= 0; i--) {
        result += num[i]
    }
    return +result
}

function forEach(arr, func) {
    for (let temp of arr) {
        func(temp)
    }
}

function map(arr, func) {
    for (let i = 0; i < arr.length; i++) {
        arr[i] = func(arr[i])
    }
    return arr;
}

function filter(arr, func) {
    let result = []
    forEach(arr, el => {
        if (func(el)) {
            result.push(el)
        }
    })
    return result
}

function getAdultAppleLovers(data) {
    let result;
    result = filter(data, el => el.age > 18 && el.favoriteFruit === 'apple')
    result = map(result, el => el.name)
    return result
}

function getKeys(obj) {
    let result = []
    for (let key in obj) {
        result.push(key)
    }
    return result
}

function getValues(obj) {
    let result = []
    for (let key in obj) {
        result.push(obj[key])
    }
    return result
}

function showFormattedDate(dateObj) {
    const MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    return `It is ${dateObj.getDate()} of ${MONTHS[dateObj.getMonth()]}, ${dateObj.getFullYear()}`
}
