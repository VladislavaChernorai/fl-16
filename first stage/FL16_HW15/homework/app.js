const root = document.getElementById('root');

const button1 = document.getElementById('navigationButtons');
button1.addEventListener('click', addTweet);
function addTweet(){
    document.getElementById('tweetItems').style.display = 'none';
    document.getElementById('modifyItem').style.visibility = 'visible';
    document.getElementById('modifyItemHeader').textContent = 'Add Tweet';
}

const button2 = document.getElementById('saveModifiedItem');
button2.addEventListener('click', saveChange);
function saveChange(){
    let archive = [];
    let tweet = document.getElementById('modifyItemInput').value;
    if (tweet){
        archive.push(tweet);
        localStorage.setItem('items', JSON.stringify(archive));
        let saved = JSON.parse(localStorage.getItem('items'));
        document.getElementById('tweetItems').style.display = 'block';
        document.getElementById('modifyItem').style.visibility = 'hidden';
        document.getElementById('modifyItemHeader').textContent = 'Simple Twitter'
        document.getElementById('list').innerHTML = '<li>' + saved + '</li>';
        document.getElementById('modifyItemInput').value = '';
        return archive;
    }
}


const textarea = document.querySelector('textarea');
const list = document.getElementById('list');
const input = document.getElementById('modifyItemInput');
let itemsArray = localStorage.getItem('items') ? JSON.parse(localStorage.getItem('items')) : [];

localStorage.setItem('items', JSON.stringify(itemsArray));
const data = JSON.parse(localStorage.getItem('items'));

const liMaker = (text) => {
    const li = document.createElement('li');
    li.textContent = text;
    list.appendChild(li);
}

textarea.addEventListener('submit', function (e) {
    e.preventDefault();

    itemsArray.push(input.value);
    localStorage.setItem('items', JSON.stringify(itemsArray));
    liMaker(input.value);
    input.value = '';
});

data.forEach(item => {
    liMaker(item);
});

button2.addEventListener('click', function () {
    localStorage.clear();
    while (list.firstChild) {
        list.removeChild(list.firstChild);
    }
    itemsArray = [];
});
