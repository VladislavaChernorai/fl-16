'use strict';

let am = +prompt('Initial amount: ');
let y = +prompt('Number of years: ');
let perc = +prompt('Percentage of year: ');

if (isNaN(am) || isNaN(y) || isNaN(perc) ||
    am < 1000 ||
    y < 1 ||
    perc > 100 ||
    (y ^ 0) !== y) {
    alert('Invalid input data');

} else {
    let total_amount = am;
    for(let j = 0; j < y; j++) {
        total_amount += total_amount / 100 * perc;
    }
    alert(
`Initial amount: ${am}
Number of years: ${y}
Percentage of year: ${perc}
    
Total profit: ${(total_amount - am).toFixed(2)}
Total amount: ${total_amount.toFixed(2)}`)
}