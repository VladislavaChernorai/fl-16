'use strict';
const t = true;

while (t){
    let min = 0;
    let max = 8;
    let attempts = 3;
    let counter = 0;
    let max_price = 100;

    let want_play = confirm('Do you want to play a game?');

    if (!want_play) {
        alert('You did not become a billionaire, but can.');
        break
    } else {
        let prize_is = max_price;

        let result = Math.floor(Math.random() * (max - min + 1)) + min;

        while (attempts !== 0) {
            let num = +prompt(`Choose a roulette pocket number from ${min} to ${max}
Attempts left: ${attempts}
Total prize: ${counter}$
Possible prize on current attempt: ${prize_is}$`);

            if(num === result) {
                counter += prize_is;

                let cont = confirm(`Congratulation, you won! Your prize is: ${counter}$. Do you want to continue?`)
                if(!cont) {
                    alert(`Thank you for your participation. Your prize is: ${counter}$`);
                    break;
                }
            else {
                    max += 4;
                    max_price *= 2;
                    attempts = 3;
                    prize_is = max_price;
                    result = Math.floor(Math.random() * (max - min + 1)) + min;
                }
            } else {
                attempts--;
                prize_is /= 2;
            }
        }
        if(attempts === 0) {
            alert(`Thank you for your participation. Your prize is: ${counter}$`);
        }
    }
}