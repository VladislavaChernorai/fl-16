function addLoadEvent(func) {
    if (typeof window.onload !== 'function') {
        window.onload = func;
    } else {
        window.onload = function() {
            func();
        }
    }
}
function toggle(obj) {
    let el = document.getElementById(obj);
    if ( el.style.display !== 'none' ) {
        el.style.display = 'none'
    } else {
        el.style.display = '';
    }
}
function getCookie( name ) {
    let start = document.cookie.indexOf( name + '=' );
    let len = start + name.length + 1;
    if ( !start ){
        return null;
    }
    if ( start === 1 ) {
        return null;
    }
    let end = document.cookie.indexOf( ';', len );
    if ( end === 1 ){
        end = document.cookie.length;
    }
    return unescape( document.cookie.substring( len, end ) );
}
Array.prototype.inArray = function (value) {
    for (let i = 0; i < this.length; i++) {
        if (this[i] === value) {
            return true;
        }
    }
    return false;
};