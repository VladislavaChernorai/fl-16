let sockets = [];
let ws;
ws.on('connection', function(w){

    let id = w.upgradeReq.headers['sec-websocket-key'];
    console.log('New Connection id :: ', id);
    w.send(id);
    w.on('message', function(msg){
        let id = w.upgradeReq.headers['sec-websocket-key'];
        let message = JSON.parse(msg);

        sockets[message.to].send(message.message);

        console.log('Message on :: ', id);
        console.log('On message :: ', msg);
    });

    w.on('close', function() {
        let id = w.upgradeReq.headers['sec-websocket-key'];
        console.log('Closing :: ', id);
    });

    sockets[id] = w;
});
